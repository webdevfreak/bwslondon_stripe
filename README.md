# Stripe payment gateway module.

### Description

This module allows to make (donation) payments of fixed/variable amounts, single 
and recurring payments by sending to Stripe website. 

### Requirements

- Drupal 8.3.x

### Installation

Since the module requires an external library (Stripe), Composer must be used.

composer require stripe/stripe-php

### Configuration

Log into your Stripe.com account and visit the "Account settings" area. Click
on the "API Keys" icon, and copy the values for Test Secret Key,
Test Publishable Key, Live Secret Key, and Live Publishable Key and paste them
into the module configuration under admin/config/stripe.
