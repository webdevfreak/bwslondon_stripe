(function ($) {

  $(document).ready(function () {

    try {
        var stripe = Stripe(drupalSettings.stripe_process.stripe_public_apikey);        
        stripe.redirectToCheckout({
          // Make the id field from the Checkout Session creation API response
          // available to this file, so you can provide it as parameter here
          // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
          sessionId: drupalSettings.stripe_process.sessionId
        }).then(function (result) {
          // If `redirectToCheckout` fails due to a browser or network
          // error, display the localized error message to your customer
          // using `result.error.message`.
          // alert('ERROR');
        });
      }
      catch(err) {
        document.getElementById("demo").innerHTML = err.message;
      }

    });
  
})(jQuery);
