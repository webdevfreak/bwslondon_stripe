<?php

/**
 * @file
 * bwslondon_stripe.views.inc
 */

/**
 * Implements hook_views_data().
 */
function bwslondon_stripe_views_data() {
  // Clear all database and static caches and rebuild data structures.
//  drupal_flush_all_caches();
//  cache_flush();
  ###
  # Stripe Data
  ###
  // Define the return array.
  $data = array();

  // The outermost keys of $data are Views table names, which should usually
  // be the same as the hook_schema() table names.
  $data['bwslondon_stripe'] = array();

  // The value corresponding to key 'table' gives properties of the table
  // itself.
  $data['bwslondon_stripe']['table'] = array();

  // Within 'table', the value of 'group' (translated string) is used as a
  // prefix in Views UI for this table's fields, filters, etc. When adding
  // a field, filter, etc. you can also filter by the group.
  $data['bwslondon_stripe']['table']['group'] = t('Stripe Data');

  // Within 'table', the value of 'provider' is the module that provides schema
  // or the entity type that causes the table to exist. Setting this ensures
  // that views have the correct dependencies. This is automatically set to the
  // module that implements hook_views_data().
  $data['bwslondon_stripe']['table']['provider'] = 'bwslondon_stripe';

  // Some tables are "base" tables, meaning that they can be the base tables
  // for views. Non-base tables can only be brought in via relationships in
  // views based on other tables. To define a table to be a base table, add
  // key 'base' to the 'table' array:
  $data['bwslondon_stripe']['table']['base'] = [
    // Identifier (primary) field in this table for Views.
    'field' => 'id',
    // Label in the UI.
    'title' => t('Stripe Data'),
    // Longer description in the UI. Required.
    'help' => t('Stripe Data table.'),
    'weight' => -10,
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['id'] = [
    'title' => t('id'),
    'help' => t('Record ID field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['stripe_session_id'] = [
    'title' => t('Stripe Session ID'),
    'help' => t('Stripe Session ID plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['stripe_pay_confirm'] = [
    'title' => t('Stripe Payment Status'),
    'help' => t('Stripe Payment Status plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ]; 

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['donation_type'] = [
    'title' => t('Donation Type'),
    'help' => t('Donation Type plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];  

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['reason_for_donation'] = [
    'title' => t('Reason for donation'),
    'help' => t('Reason for donation plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['other_reason'] = [
    'title' => t('Other reason'),
    'help' => t('Other reason plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['amount'] = [
    'title' => t('Amount'),
    'help' => t('Amount plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['first_name'] = [
    'title' => t('First Name'),
    'help' => t('First Name plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['last_name'] = [
    'title' => t('Last Name'),
    'help' => t('Last Name plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['email'] = [
    'title' => t('Email'),
    'help' => t('Email plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['email_signup'] = [
    'title' => t('Email signup'),
    'help' => t('Email signup plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['address_1'] = [
    'title' => t('Address Line 1'),
    'help' => t('Address Line 1 plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['address_2'] = [
    'title' => t('Address Line 2'),
    'help' => t('Address Line 2 plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['city'] = [
    'title' => t('City'),
    'help' => t('City plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['postcode'] = [
    'title' => t('Postcode'),
    'help' => t('Postcode plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['country'] = [
    'title' => t('Country'),
    'help' => t('Country plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['gift_aid'] = [
    'title' => t('Gift Aid'),
    'help' => t('Gift Aid plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'int',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'int',
    ],
  ];

  // Exposed as a field, sort, filter, and argument.
  $data['bwslondon_stripe']['created'] = [
    'title' => t('Created'),
    'help' => t('Created plain text field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'date',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'date',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'date',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'date',
    ],
  ];

  return $data;
}
