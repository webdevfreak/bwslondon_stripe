<?php
/**
 * @file
 * Contains \Drupal\bwslondon_stripe\Controller\StripeResultController.
 */
namespace Drupal\bwslondon_stripe\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Component\Utility\Xss;

class StripeResultController extends ControllerBase {

  public function bwslondon_stripe_result(){
    // Get id from url.
    $stripe_session_id = Xss::filter(bwslondon_stripe_get_id_from_url(4));

    // Update database record and mark this transaction as confirmed.
    $query = db_merge('bwslondon_stripe')
      ->key(array('stripe_session_id' => $stripe_session_id))
      ->fields(array(
        'stripe_pay_confirm' => 'Yes',
      ))
      ->execute();

    // Get all data from the database based on session id.
    $result = db_select('bwslondon_stripe', 'n')
      ->fields('n')
      ->condition('stripe_session_id', $stripe_session_id, '=')
      ->execute()
      ->fetchObject();

    // Donation detail array.
    $donation_detail = [
      'stripe_session_id' => $stripe_session_id,
      'donation_type' => $result->donation_type,
      'reason_for_donation' => $result->reason_for_donation,
      'other_reason' => $result->other_reason,
      'amount' => $result->amount,
      'first_name' => $result->first_name,
      'last_name' => $result->last_name,
      'phone' => $result->phone,
      'email' => $result->email,
      'email_signup' => $result->email_signup,
      'address_1' => $result->address_1,
      'address_2' => $result->address_2,
      'city' => $result->city,
      'postcode' => $result->postcode,
      'country' => $result->country,
      'gift_aid' => $result->gift_aid,
      'created' => $result->created,
    ];

    // Send email to admin.
    $to = 'info@bwslondon.co.uk';
    $subject = 'Donation notification';
    bwslondon_stripe_send_email($to, $subject, $donation_detail);

    // Redirect to success page.
    $response = new RedirectResponse('/donation-successful');
    $response->send();

    return ['#markup' => $stripe_session_id];
  }

}
