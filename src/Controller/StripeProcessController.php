<?php

/**
 * @file
 * Contains \Drupal\bwslondon_stripe\Controller\StripeProcessController.
 */

namespace Drupal\bwslondon_stripe\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;
use Symfony\Component\HttpFoundation\RedirectResponse;

class StripeProcessController extends ControllerBase {

  public function bwslondon_stripe_process(){
    // Attach Stripe library.
    $page['#attached']['library'][] = 'bwslondon_stripe/bwslondon_stripe.stripe_process';

    // Get donation form posted data.
    $request = Request::createFromGlobals();

    // Monthly or single.
    $donation_type = Xss::filter($request->get('donation_type'));
    
    // Set default values.
    $amount = 1000;
    $other_amount = 1;

    // Amount.
    if(Xss::filter($request->get('amount'))){
      $amount = Xss::filter($request->get('amount')) * 100;
    }

    // Other amount.
    if(Xss::filter($request->get('amountval'))){
      // Assign posted value to a variable.
      $other_amount = Xss::filter($request->get('amountval'));
      
      // Replace currency symbols.
      $other_amount = str_replace('£', '', $other_amount);
      $other_amount = str_replace('GBP', '', $other_amount);
      $other_amount = str_replace('$', '', $other_amount);
      $other_amount = str_replace('€', '', $other_amount);

      // Set this variable as integer.
      // settype($other_amount, 'integer');
      settype($other_amount, 'float');

      // Set £10 if amount variable is empty.
      if(!$other_amount || $other_amount == 0){
        $amount = 1000;
      }else{
        $amount = $other_amount * 100;
      }
    } 

    // Disabled because day drop down field on the donation form is disabled.
    // Linked with day drop down field on the donation form.
    // Calculate start day value.
    /* $day_start = Xss::filter($request->get('day_start'));
    $cal_days = cal_days_in_month(CAL_GREGORIAN,date('m'),date('Y'));
    print($cal_days.'<br>');
    $cal_days = $cal_days - $day_start;
    print($cal_days.'<br>');
    if($day_start < date('d')){
      $day_start = $cal_days + $day_start;
    }
	
    print($cal_days.'<br>');
    if(!$day_start){
      $day_start = '1';
    }*/

    // Error control.
    if(!$request->get('donation_type')){
      // Redirect to the home page.
      $response = new RedirectResponse('/');
      $response->send();
    }

    // Initialise variables.
    $apikey_environment = NULL;
    $apikey_public_test = NULL;
    $apikey_secret_test = NULL;
    $apikey_public_live = NULL;
    $apikey_secret_live = NULL;

    // Get Stripe API key values from admin/config/stripe.
    $apikey_environment = \Drupal::config('bwslondon_stripe.settings')->get('environment');
    $apikey_public_test = \Drupal::config('bwslondon_stripe.settings')->get('apikey_public_test');
    $apikey_secret_test = \Drupal::config('bwslondon_stripe.settings')->get('apikey_secret_test');
    $apikey_public_live = \Drupal::config('bwslondon_stripe.settings')->get('apikey_public_live');
    $apikey_secret_live = \Drupal::config('bwslondon_stripe.settings')->get('apikey_secret_live');

    // Error control.
    // Check if all key values are available otherwise redirect to home page.
    if(!$apikey_public_test || 
       !$apikey_secret_test ||
       !$apikey_public_live ||
       !$apikey_secret_live
     ){
      $key_data = [
        'markup' => 'All API key values must be provided at /admin/config/stripe 
         section.',
      ];
      // Redirect to the home page.
      $response = new RedirectResponse('/');
      $response->send();
    }

    // Set correct API key.
    $stripe_secret_apikey = ($apikey_environment == 'test') ? $apikey_secret_test : $apikey_secret_live;
    $stripe_public_apikey = ($apikey_environment == 'test') ? $apikey_public_test : $apikey_public_live;
    
    // Make Stripe API call and process.
    \Stripe\Stripe::setApiKey($stripe_secret_apikey);

    // Process donation type.
    switch ($donation_type) {
      case 'Single':
        $session = \Stripe\Checkout\Session::create([
          'payment_method_types' => ['card'],
          'line_items' => [[
            'name' => 'Donation',
            'description' => $donation_type,
            'amount' => $amount,
            'currency' => 'gbp',
            'quantity' => 1,
          ]],          
          'success_url' => 'https://www.domain.org.uk/donate/stripe/result/{CHECKOUT_SESSION_ID}',
          'cancel_url' => 'https://www.domain.org.uk/payment-failed',
        ]);
        break;

      case 'Monthly':
        $plan_amount = $amount / 100;
        $plan_amount = number_format((float)$plan_amount, 2, '.', '');

        // Create a plan..
        $plan = \Stripe\Plan::create([
          'amount' => $amount,
          'currency' => 'gbp',
          'interval' => 'month',
          'product' => ['name' => 'Recurring Plan £' . $plan_amount],
          'trial_period_days' => '0',
        ]);

        // Create a Checkout Session.
        $session = \Stripe\Checkout\Session::create([
          'payment_method_types' => ['card'],
          'subscription_data' => [
            'items' => [[
              'plan' => $plan->id,
            ]],
            'trial_from_plan' => true,
          ],
          'success_url' => 'https://www.domain.org.uk/donate/stripe/result/{CHECKOUT_SESSION_ID}',
          'cancel_url' => 'https://www.domain.org.uk/payment-failed/payment-failed',
        ]);
        break;
    }

    // Convert donation amount for saving in the database table.
    $donation_amount = $amount / 100;
    $donation_amount = number_format($donation_amount, 2, '.', '');

    // Update email signup variable.
    $email_signup = Xss::filter($request->get('email_signup'));
    if(Xss::filter($request->get('email_signup')) <> 'Yes'){
      $email_signup = 'No';
    }

    // Update gift_aid variable.
    $gift_aid = Xss::filter($request->get('gift_aid'));
    if(Xss::filter($request->get('gift_aid')) <> 'Yes'){
      $gift_aid = 'No';
    }

    // Save posted data in the database table.
    $query = db_insert('bwslondon_stripe')
      ->fields(array(
        'stripe_session_id' => $session->id,
        'donation_type' => $donation_type,
        'reason_for_donation' => Xss::filter($request->get('reason_for_donation')),
        'other_reason' => Xss::filter($request->get('other_reason')),
        'amount' => $donation_amount,
        'first_name' => Xss::filter($request->get('fname')),
        'last_name' => Xss::filter($request->get('lname')),
        'phone' => Xss::filter($request->get('phone')),
        'email' => Xss::filter($request->get('email')),
        'email_signup' => $email_signup,
        'address_1' => Xss::filter($request->get('address1')),
        'address_2' => Xss::filter($request->get('address2')),
        'city' => Xss::filter($request->get('city')),
        'postcode' => Xss::filter($request->get('postcode')),
        'country' => Xss::filter($request->get('country')),
        'gift_aid' => $gift_aid,
        'created' => REQUEST_TIME,
      ))
      ->execute();
    
    // Pass dynamic values to Stripe js.
    // Redirect to Stripe to take payment.    
    $page['#attached']['drupalSettings']['stripe_process']['sessionId']= isset($session->id) ? $session->id : ''; 
    $page['#attached']['drupalSettings']['stripe_process']['stripe_public_apikey']= $stripe_public_apikey; 
    ?>

    <p id="demo"></p>    

    <?php
    return $page;
  }
}
